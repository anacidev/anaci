package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Commande extends BaseModel{
	
	@ManyToOne
	private Client client;
	private long QteCmde;
	private int statutCmde; // 0 pour rejeter, 1 pour en attente, 2 pour validé
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public long getQteCmde() {
		return QteCmde;
	}
	public void setQteCmde(long qteCmde) {
		QteCmde = qteCmde;
	}
	public Commande() {
		super();
	}
	
	
	

}
