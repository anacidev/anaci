package models;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType; 

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import javax.persistence.ManyToOne;
@Entity
public class FicheStock extends BaseModel{
	private long qteEntre;
	private long priceEnter;
	private long montantEntre;
	private long qteSortie;
	private long priceSortie;
	private long montantSortie;
	private long stockFinal;
	@ManyToOne
	private Client client;
	@ManyToOne
	private Membre membre;
	public long getQteEntre() {
		return qteEntre;
	}
	public void setQteEntre(long qteEntre) {
		this.qteEntre = qteEntre;
	}
	public long getPriceEnter() {
		return priceEnter;
	}
	public void setPriceEnter(long priceEnter) {
		this.priceEnter = priceEnter;
	}
	public long getMontantEntre() {
		return montantEntre;
	}
	public void setMontantEntre(long montantEntre) {
		this.montantEntre = montantEntre;
	}
	public long getQteSortie() {
		return qteSortie;
	}
	public void setQteSortie(long qteSortie) {
		this.qteSortie = qteSortie;
	}
	public long getPriceSortie() {
		return priceSortie;
	}
	public void setPriceSortie(long priceSortie) {
		this.priceSortie = priceSortie;
	}
	public long getMontantSortie() {
		return montantSortie;
	}
	public void setMontantSortie(long montantSortie) {
		this.montantSortie = montantSortie;
	}
	public long getStockFinal() {
		return stockFinal;
	}
	public void setStockFinal(long stockFinal) {
		this.stockFinal = stockFinal;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Membre getMembre() {
		return membre;
	}
	public void setMembre(Membre membre) {
		this.membre = membre;
	}
	public FicheStock() {
		super();
	}
	
	

}
