package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Compte extends BaseModel{
	private String login;
	private String password;
	private int statutCpte;
	private String email;
	private String telephone;
	
	public Compte() {
		super();
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatutCpte() {
		return statutCpte;
	}
	public void setStatutCpte(int statutCpte) {
		this.statutCpte = statutCpte;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	




	

}
