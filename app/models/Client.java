package models;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Client extends Utilisateur {
	private String raison_sociale;
	private String statut_juridique;
	private String localisation;
	public String getRaison_sociale() {
		return raison_sociale;
	}
	public void setRaison_sociale(String raison_sociale) {
		this.raison_sociale = raison_sociale;
	}
	public String getStatut_juridique() {
		return statut_juridique;
	}
	public void setStatut_juridique(String statut_juridique) {
		this.statut_juridique = statut_juridique;
	}
	public String getLocalisation() {
		return localisation;
	}
	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}
	public Client() {
		super();
	}
	
	
	

}
