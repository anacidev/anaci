package models;

import java.util.Date;



import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType; 
import java.util.*;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Membre extends Utilisateur {
	 @Temporal(TemporalType.TIMESTAMP)
	 @JsonProperty
	private Date birthday;
	private String lieu_Naiss;
	private String live_place;
	
	public Date getBirthday() {
		return birthday;
	}
	
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getLieu_Naiss() {
		return lieu_Naiss;
	}
	public void setLieu_Naiss(String lieu_Naiss) {
		this.lieu_Naiss = lieu_Naiss;
	}
	public String getLive_place() {
		return live_place;
	}
	public void setLive_place(String live_place) {
		this.live_place = live_place;
	}
	public Membre() {
		super();
	}
	
	
	
	

}


